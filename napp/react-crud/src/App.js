import logo from './logo.svg';
import './App.css';

import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import { Link, Route, Routes } from "react-router-dom";

import Home from "./components/bittyhome.component";
import UserRegister from "./components/user-register.component";

class App extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark">
          <a href="/user" className="navbar-brand">
            Bitty Napp
          </a>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to={"/user"} className="nav-link">
                List Users
              </Link>
            </li>
            <li className="nav-item">
              <Link to={"/register"} className="nav-link">
                Register
              </Link>
            </li>
          </div>
        </nav>
        <div> HELLO WORLD APP</div>
        <div className="container mt-3">
        <Routes>
          <Route exact path="/" element={<Home />} />
          <Route exact path="/register" element={<UserRegister />} />
          {//<Route path="/user/:id" component={User} />
          }
        </Routes>

        </div>

      </div>
    );
  }
}

export default App;
