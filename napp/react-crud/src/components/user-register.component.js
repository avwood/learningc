import React, { Component } from "react";
import UserDataService from "../services/user.service";

export default class UserRegister extends Component {
  constructor(props) {
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeOwnerEmail = this.onChangeOwnerEmail.bind(this);
    this.registerUser = this.registerUser.bind(this);
    this.newUser = this.newUser.bind(this);

    this.state = {
      owner_id: null,
      name: "",
      owner_email: ""
    };
  }

  onChangeName(e) {
    this.setState({
      name: e.target.value
    });
  }

  onChangeOwnerEmail(e) {
    this.setState({
      owner_email: e.target.value
    });
  }

  registerUser() {
    var data = {
      owner_id: this.owner_id,
      name: this.state.name,
      owner_email: this.state.owner_email
    };

    UserDataService.create(data)
      .then(response => {
        this.setState({
          owner_id: response.data.owner_id,
          name: response.data.name,
          owner_email: response.data.owner_email,
        });
        console.log(response.data);
      })
      .catch(e => {
        console.log(e);
      });
  }

  newUser() {
    this.setState({
      owner_id: null,
      name: "",
      owner_email: ""
    });
  }

  render() {
      console.log("REGISTER USER RENDER");
    return (
        <div>
            <div>HELLO WORLD - REGISTER</div>
            <div className="form-group">
              <label htmlFor="name">Name</label>
              <input
                type="text"
                className="form-control"
                id="name"
                required
                value={this.state.name}
                onChange={this.onChangeName}
                name="name"
              />
            </div>

            <div className="form-group">
              <label htmlFor="owner_email">Email Address</label>
              <input
                type="text"
                className="form-control"
                id="owner_email"
                required
                value={this.state.owner_email}
                onChange={this.onChangeOwnerEmail}
                name="owner_email"
              />
            </div>

            <button onClick={this.registerUser} className="btn btn-success">
              Submit
            </button>
          </div>
    )
  }
}
