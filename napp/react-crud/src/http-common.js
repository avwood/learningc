import axios from "axios";

let BASE_URL = "http://localhost:8080/api";

if (process.env.REACT_APP_DOMAIN_PROD) {
  BASE_URL = process.env.REACT_APP_DOMAIN_PROD;            
} 

export default axios.create({
  baseURL: BASE_URL,
  headers: {
    "Content-type": "application/json"
  }
});
