  const sql = require("./db.js");

  const User = function(user) {
      console.log("user constructor: "+JSON.stringify(user))
      this.name = user.name;
      this.owner_email = user.owner_email;
    }
  
    User.create = (newUser, result) => {
      console.log("user model create: "+JSON.stringify(newUser));
      sql.query("INSERT INTO users (name, owner_email) VALUES($1,$2)", [newUser.name, newUser.owner_email], (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }

        console.log("created users: ", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
      });
    };
    
    User.findById = (id, result) => {
      console.log("user model find by id "+id);

      sql.query(`SELECT * FROM users WHERE owner_id = ${id}`, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(err, null);
          return;
        }
    
        if (res.length) {
          console.log("found users: ", res[0]);
          result(null, res[0]);
          return;
        }
    
        // not found User with the id
        result({ kind: "not_found" }, null);
      });
    };
    
    User.getAll = (name, result) => {
      console.log("user model getall");
      let query = "SELECT * FROM users";
    
      if (name) {
        query += ` WHERE name LIKE '%${name}%'`;
      }
    
      sql.query(query, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
    
        console.log("users: ", res);
        result(null, res);
      });
    };
    
    User.updateById = (id, user, result) => {
      console.log("user model update by id");

      sql.query(
        "UPDATE users SET name = ?, owner_email = ? WHERE owner_id = ?",
        [user.name, user.owner_email, id],
        (err, res) => {
          if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
          }
    
          if (res.affectedRows == 0) {
            // not found User with the id
            result({ kind: "not_found" }, null);
            return;
          }
    
          console.log("updated user: ", { id: id, ...user });
          result(null, { id: id, ...user });
        }
      );
    };
    
    User.remove = (id, result) => {
      console.log("user model remove");

      sql.query("DELETE FROM users WHERE owner_id = ?", id, (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
    
        if (res.affectedRows == 0) {
          // not found User with the id
          result({ kind: "not_found" }, null);
          return;
        }
    
        console.log("deleted user with id: ", id);
        result(null, res);
      });
    };
    
    User.removeAll = result => {
      sql.query("DELETE FROM users", (err, res) => {
        if (err) {
          console.log("error: ", err);
          result(null, err);
          return;
        }
    
        console.log(`deleted ${res.affectedRows} users`);
        result(null, res);
      });
    };
    
    module.exports = User;