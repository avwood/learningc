const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

let CORS_HOST = "http://localhost:8081";

if (process.env.CORS_HOST) {
  CORS_HOST = process.env.CORS_HOST;            
} 

var corsOptions = {
  origin: CORS_HOST
};

app.use(cors(corsOptions));


// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

//const db = require("./models");
//db.sequelize.sync();

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to napp application." });
});

console.log("calling router");
require("./routes/user.routes.js")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

