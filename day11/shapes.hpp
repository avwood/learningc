#ifndef SHAPE_HPP
#define SHAPE_HPP 1
class Shape {
    protected:
        int top;
        int left;
        int bottom;
        int right;
    public:
        Shape(int top, int left, int bottom, int right);
        int getTop();
        void setTop(int i);
        int getLeft();
        void setLeft(int i);
        int getBottom();
        void setBottom(int i);
        int getRight();
        void setRight(int i);
        void readShape(ifstream &filestream);
        virtual void draw();
        virtual string getShapeName();

        //friend std::ostream& operator<<(std::ostream &out, Shape e);
};

class Rectangle:public Shape
{
    public:
        Rectangle(int top, int left, int bottom, int right);
        void readShape(ifstream &filestream);
        void draw();
        string getShapeName();
};

class Circle:public Shape
{
    public:
        Circle(int top, int left, int bottom, int right);
        void readShape(ifstream &filestream);
        void draw();
        string getShapeName();
};

#endif