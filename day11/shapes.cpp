#include <iostream>  
#include <fstream>
#include <vector>
using namespace std;
#include "shapes.hpp"


Shape::Shape(int top, int left, int bottom,int right)
{

    this->top = top;
    this->left = left;
    this->bottom = bottom;
    this->right = right;
}

int Shape::getTop()
{
    return this->top;
}
void Shape::setTop(int i)
{
   this->top = i;
}

int Shape::getLeft()
{
    return this->left;
}
void Shape::setLeft(int i)
{
   this->left = i;
}
int Shape::getBottom()
{
    return this->bottom;
}
void Shape::setBottom(int i)
{
   this->bottom = i;
}
int Shape::getRight()
{
    return this->right;
}
void Shape::setRight(int i)
{
   this->right = i;
}

void Shape::draw()
{
    printf("This is a generic shape\n");
}

string Shape::getShapeName()
{
    return "Generic Shape";
}

/*
ostream& operator<<(ostream &out, Shape* e)
{
    out << e->getShapeName();
    return out;
}
*/

Rectangle::Rectangle(int top, int left, int bottom, int right):Shape(top,left,bottom,right)
{
}

void Rectangle::draw()
{
  printf("I'm a rectangle at: %d,%d,%d,%d\n",top,left,bottom,right);
}

void Rectangle::readShape(ifstream &filestream)
{
  string snum;
  int num;
  getline (filestream,snum);
  this->setTop(stoi(snum));
  getline (filestream,snum);
  this->setLeft(stoi(snum));
  getline (filestream,snum);
  this->setBottom(stoi(snum));
  getline (filestream,snum);
  this->setRight(stoi(snum));
}

string Rectangle::getShapeName()
{
  return "Rectangle";
}

Circle::Circle(int top, int left, int bottom, int right):Shape(top,left,bottom,right)
{
}

void Circle::draw()
{
  printf("I'm a circle at: %d,%d,%d,%d\n",top,left,bottom,right);
}

void Circle::readShape(ifstream &filestream)
{
  string snum;
  int num;
  getline (filestream,snum);
  this->setTop(stoi(snum));
  getline (filestream,snum);
  this->setLeft(stoi(snum));
  getline (filestream,snum);
  this->setBottom(stoi(snum));
  getline (filestream,snum);
  this->setRight(stoi(snum));
}

string Circle::getShapeName()
{
  return "Circle";
}

Shape* readShape(ifstream &filestream)
{
    string shapename;
    if (getline (filestream,shapename))
    {
      if (shapename.compare("Circle")==0) 
      {
        cout << "Shape is Circle"<<endl;
        Circle *circ = new Circle(0,0,0,0);
        circ->readShape(filestream);
        return circ;
      }
      else if (shapename.compare("Rectangle")==0)
      {
        cout << "Shape is Rectangle"<<endl;
        Rectangle *rect = new Rectangle(0,0,0,0);
        rect->readShape(filestream);
        return rect;
      }
      else
      {
        cout << "Unknown shape type: " << shapename << endl;
      }
    }  
    return NULL;
}

int main () {  
  Shape *tmpShape;
  vector<Shape*> vshapes;
  string srg;  
  int i = 0;
  ifstream filestream("drawing.txt"); 
  if (filestream.is_open())  
  { 

    tmpShape = readShape(filestream);
    while (tmpShape!=NULL)
    
    {  
      vshapes.push_back(tmpShape);

      tmpShape = readShape(filestream);
    }  
    
    filestream.close();  
  }  
  else {  
      cout << "File opening is fail."<<endl;   
    }

  for(vector<Shape*>::iterator itr=vshapes.begin();itr!=vshapes.end();++itr) 
     (*itr)->draw();

  return 0;  
}  