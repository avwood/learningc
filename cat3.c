#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
  char buffer[1024];

  /*
  printf("argc=%d\n", argc);
  int i;
  for (i=0;i<argc;i++)
  {
    printf("arg[%d] = %s\n", i, argv[i]);
  }
  */
  int fd;

  if (argc==2)
    {
      fd = open(argv[1], O_RDONLY);
    }
  else if (argc==1)
    {
      fd = 0;
    }
  else
    {
      printf("usage: cat3 [filename]\n");
      return -1;
    }

  int len=1;
  
  while (len>0)
  {
    len = read(fd, &buffer, 1);
    if (len>0)
    {
      write(1, &buffer, 1);
    }
  }
}
