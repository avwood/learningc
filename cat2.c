#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main()
{
  char buffer[1024];

  int fd = open("data.txt", O_RDONLY);

  int len=1;
  
  while (len>0)
  {
    len = read(fd, &buffer, 1);
    if (len>0)
    {
      write(1, &buffer, 1);
    }
  }
}
