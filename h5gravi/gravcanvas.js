var canvas = document.getElementById('gravcanvas');
var ctx = canvas.getContext('2d');
if (!canvas.getContext) {  
  alert('Canvas not supported');
}

const TO_RADS = 2*Math.PI / 360.0;

var ship_pos = {"x":25, "y":25};
var sun_pos = {"x": 512, "y": 512};
var ship_velocity = {"x": 0.0, "y": 0.0};
var ship_rotating_left = false;
var ship_rotating_right = false;
var ship_thrust = false;
var ship_rotation = 0.0;

function getDistance(x1, y1, x2, y2){
    let y = x2 - x1;
    let x = y2 - y1;
    
    return Math.sqrt(x * x + y * y);
}



var ship_outline = [
    {"x":0,"y":-5},
    {"x":-3,"y":5},
    {"x":0,"y":2},
    {"x":3,"y":5},
];




function draw_ship(p, r) {
    let firstpoint = true;
    ctx.beginPath();
    for (let i=0;i<ship_outline.length;i++) {
        let curp = rotate(ship_outline[i], r);
        if (firstpoint) {
            ctx.moveTo(p.x+curp.x,p.y+curp.y);
            firstpoint = false;
        } else {
            ctx.lineTo(p.x+curp.x,p.y+curp.y);
        }
    }
    ctx.closePath();
    ctx.stroke();
}

function draw_sun(){
    ctx.beginPath();
    ctx.arc(sun_pos.x, sun_pos.y, 75, 0, 2 * Math.PI);
    ctx.stroke();
}

function rotate(p, r) {
    var newp = {
        "x":(p.x * Math.cos(r) - p.y*Math.sin(r)),
        "y":(p.x * Math.sin(r) + p.y*Math.cos(r))
    }
    return newp;
}

function bounce_x() {
    ship_velocity.x = -ship_velocity.x * 1;
}

function bounce_y() {
    ship_velocity.x = ship_velocity.x * 1;
    ship_velocity.y = -ship_velocity.y * 1;
}

function refresh() {
    // clear the canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    draw_ship(ship_pos, ship_rotation*TO_RADS);
    draw_sun();
}

function tick() {


//key commands

    if (ship_rotating_left) {
        ship_rotation -= 15;
    } else if (ship_rotating_right) {
        ship_rotation += 15;
    }

    if (ship_thrust) {
        var unit_up = {"x": 0.0, "y": -1};
        var ship_dir = rotate(unit_up, ship_rotation*TO_RADS);
        ship_velocity.x += ship_dir.x;
        ship_velocity.y += ship_dir.y;
    }

//bounce off walls

    if (ship_pos.x>1029 || ship_pos.x<2) {
        
        //grav = {"x":0.0, y:0.0};
        bounce_x();
    }

    if (ship_pos.y>1029 || ship_pos.y<5) {
        
        //grav = {"x":0.0, y:0.0};
        bounce_y();
    }



    // gravity for the sun
    var a =  sun_pos.y - ship_pos.y; // I used a and b as in the sides of a right triangele in a^2 + b^2 = c^2
    var b =  sun_pos.x - ship_pos.x;
    var theta = Math.atan(a/b);
    var grav = {"x": Math.cos(theta), "y": Math.sin(theta),};


    //bounce off sun (has to be here or the sun will catch the ship)
    var dist= getDistance(ship_pos.x, ship_pos.y, sun_pos.x, sun_pos.y)

    if (dist < 80) {
        bounce_x();
        bounce_y();
        gravgrav = {"x": 0.0, "y": 0.0,};
        
    }

    if (ship_pos.x < sun_pos.x) {
        ship_velocity.x += (5/(Math.sqrt(dist))) * grav.x;
        ship_velocity.y += (5/(Math.sqrt(dist))) * grav.y;
    }   else if (ship_pos.x > sun_pos.x) {
        ship_velocity.x -= (5/(Math.sqrt(dist))) * grav.x;
        ship_velocity.y -= (5/(Math.sqrt(dist))) * grav.y;
    }






    ship_pos.x += ship_velocity.x;
    ship_pos.y += ship_velocity.y;
    
    ship_pos.x += grav.x;
    ship_pos.y += grav.y;


    refresh();
}

function animate() {
    setInterval(tick, 100);
 }

function grav_main() {

    console.log("Starting script");

    animate();
}

function keydown_event(e) {
    //alert(e.keyCode);
    if (e.keyCode==74) { // j
        ship_rotating_left = true;
    } else if (e.keyCode==75) {
        ship_thrust = true
    } else if (e.keyCode==76) { // l
        ship_rotating_right = true;
    }
}

function keyup_event(e) {
    //alert(e.keyCode);
    if (e.keyCode==74) { // j
        ship_rotating_left = false;
    } else if (e.keyCode==75) {
        ship_thrust = false
    } else if (e.keyCode==76) { // l
        ship_rotating_right = false;
    }
}
window.addEventListener('keydown',keydown_event,false);
window.addEventListener('keyup',keyup_event,false);


grav_main();
