#include <string.h>
#include <stdio.h>
int main()
{
    int i;

    // show 10 times
    for(i=0;i<10;i++) 
    {
      printf("hello world: %d\n", i);
      if (i==8) 
      {
        printf("We reached 8!\n");
      }
    }

    // lets do this again
    int running = 1;
    i = 0;
    while (running>0) 
    {
      printf("hello world2\n");
      if (i>11) 
      {
        running = 0;
      }
      i++;
    }

    char mystr[32];
    int j=0;
    int k=0;
    mystr[33] = 'a';
    printf("j=%d  k=%d\n", j, k);

    strcpy(mystr,"hello2");
    mystr[0] = 'a';
    mystr[1] = '\0';
    printf("h2=%s\n", mystr);
}
