
# File Backup

## Purpose

The purpose of this application is to monitor an dbackup one folder to another and eventually to backup to the cloud. This folder would also need an easy ability to restore as well.

## Key Features

1. Watch a folder structure for changes between the source and backup.
1. Send changed files to the backup folder.
1. Versioning in the backup folder to protect from deletion and allow recovery.
1. Build in a web service to have the backup folder stored remote (most likely S3)

## First steps

Come up with an algorithm to backup one folder to another

## Algorithm

1. Create a list of files in the source folder and a list in the backup folder

## File list

Eventually the backup folder will be remote and for speed purposes we don't want to make every file check a call to a remote server.  We need some sort of file listing that we can build up (or download from the remote backup) and quickly check against that local list of files vs. making a local check of the file system or remote we service for every call.

```
BBackFileInfo
  path
  filename
  size
  checksum

BackupSystemFileList
  Hash<String><Hash>(path)[Hash<String><BBackFileInfo>(filename)]
  BuildFileList(path:<String>);
  CompareSourceDest(FileInfo source) -> bool
```




[This is a link](https://www.archive.org)
