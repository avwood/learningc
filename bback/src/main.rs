
use crypto::digest::Digest;
use crypto::sha1::Sha1;
use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::HashMap;
use std::collections::LinkedList;
use std::env;
use std::fs;
use std::fs::File;
use std::os::unix::fs::PermissionsExt;
use std::fs::ReadDir;
use std::io::Read;







#[derive(Debug)]
struct BBFileInfo 
{
    path: String,
    filename: String,
    permissions: u32,
    size: usize,
    checksum_valid: bool,
    checksum: String,
}

#[derive(Debug)]
enum Event
{
    WholeBackup,
    FolderBackup,
    PartialBackup,
    Delete,
    PermissionChange,

}

#[derive(Debug)]
struct BBWork
{
    event_type: Event,
    path: String,
    filename: String,
    permissions: u32,
}

#[derive(Debug)]
struct BBBackupSystemFileList 
{
     folderlist: HashMap<String, RefCell<HashMap<String, RefCell<BBFileInfo>>>>,
     work_list: LinkedList<BBWork>,
}


const BUFFER_SIZE: usize = 1024;
fn calc_checksum<D: Digest, R: Read>(reader: &mut R, sh: &mut D) {
    let mut buffer = [0u8; BUFFER_SIZE];
    loop 
    {
        let n = match reader.read(&mut buffer) 
        {
            Ok(n) => n,
            Err(_) => return,
        };
        sh.input(&buffer[..n]);
        if n == 0 || n < BUFFER_SIZE 
        {
            break;
        }
    }
}

fn build_file_list(s_folder_path: &String, rd_file: ReadDir, bbbs_folders: &mut BBBackupSystemFileList)
{
    for entry in rd_file
    {

        let entry = entry.unwrap();

        let path = entry.path();

        let ref_filename= entry.file_name();

        if path.is_dir()
        {
            build_file_list(&format!("{}{}/", s_folder_path, path.file_name().unwrap().to_str().unwrap()) ,fs::read_dir(&path).unwrap(), bbbs_folders);
        }

        else
        {
            let mut fi=BBFileInfo
            {
                path: String::from(path.to_str().unwrap()),
                filename: String::from(ref_filename.to_str().unwrap()),
                permissions: entry.metadata().unwrap().permissions().mode(),
                size: 0,
                checksum_valid: false,
                checksum: String::from("none yet"),
            };

            let ref_fi:RefCell<&mut BBFileInfo>=RefCell::new(&mut fi);

            if let Ok(mut file) = fs::File::open(&path) 
            {
                let mut cs = Sha1::new();
                calc_checksum(&mut file, &mut cs);
                ref_fi.borrow_mut().checksum = cs.result_str();

            }


            if  bbbs_folders.folderlist.contains_key(s_folder_path)
            {
                let little_hash= bbbs_folders.folderlist.get(s_folder_path).unwrap();
                little_hash.borrow_mut().insert(String::from(ref_filename.to_str().unwrap()), RefCell::new(fi));
            }
            else
            { 
                let mut little_hash=HashMap::new();
                little_hash.insert(String::from(ref_filename.to_str().unwrap()), RefCell::new(fi));
                bbbs_folders.folderlist.insert(s_folder_path.to_string(),RefCell::new(little_hash));
                
            }

            

        }

    }   
}

#[test]
fn file_list_built()
{
    let mut from_list = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
        work_list : LinkedList::new(),
    };

    build_file_list(&String::from("/"),fs::read_dir(String::from("from")).unwrap(), &mut from_list);

    println!("folderlist length: {}", from_list.folderlist.len());

    assert_ne!(from_list.folderlist.len(), 0);
   //assert_eq!(from_list.work_list.len(), 1);

}

fn compare_hash(bbbs_folders_from: &mut BBBackupSystemFileList, bbbs_folders_to: &mut BBBackupSystemFileList)
{
    for (from_big_hash_key, from_big_hash_value) in &bbbs_folders_from.folderlist
    {
        let mut folder_exists=false;

        if bbbs_folders_to.folderlist.borrow().contains_key(from_big_hash_key)
        {
            let little_to_hash=bbbs_folders_to.folderlist.get(from_big_hash_key).unwrap();

            folder_exists=false;
            for (from_little_hash_key, from_little_hash_value) in from_big_hash_value.borrow_mut().iter_mut()
            {
                if little_to_hash.borrow_mut().contains_key(from_little_hash_key)
                { 
                    println!("File already exists in backup directory: {}",from_little_hash_key);

                    let from_fileinfo= from_little_hash_value.borrow_mut();

                    if from_fileinfo.checksum !=little_to_hash.borrow_mut().get(from_little_hash_key).unwrap().borrow().checksum
                    {
                        let fi_work=BBWork
                        {
                            event_type: Event::PartialBackup,
                            path: String::from(from_big_hash_key),
                            filename: String::from(from_little_hash_key),
                            permissions: from_fileinfo.permissions,
                        };
    
                        bbbs_folders_from.work_list.push_back(fi_work);
                        println!("File has been changed: {}", from_little_hash_key);
                    }

                    if from_fileinfo.permissions != little_to_hash.borrow_mut().get(from_little_hash_key).unwrap().borrow().permissions
                    {
                        let fi_work=BBWork
                        {
                            event_type: Event::PermissionChange,
                            path: String::from(from_big_hash_key),
                            filename: String::from(from_little_hash_key),
                            permissions: from_fileinfo.permissions,
                        };
    
                        bbbs_folders_from.work_list.push_back(fi_work);
                        println!("permissions have changed: {}", from_little_hash_key);
                    }
                }
            

                else
                {
                    println!("Copy file into backup directory: {}",from_little_hash_key);
                    println!("path: {}",from_big_hash_key);
                    let fi_work=BBWork
                    {
                        event_type: Event::WholeBackup,
                        path: String::from(from_big_hash_key),
                        filename: String::from(from_little_hash_key),
                        permissions: from_little_hash_value.borrow_mut().permissions,
                    };

                    bbbs_folders_from.work_list.push_back(fi_work);
                }
            }
            
        }
        if folder_exists==false
        {
            println!("Copy folder into backup directory: {}", from_big_hash_key);
            let fi_work=BBWork
                    {
                        event_type: Event::FolderBackup,
                        path: String::from(from_big_hash_key),
                        filename: String::from("folder"),
                        permissions:7,
                    };

                    bbbs_folders_from.work_list.push_back(fi_work);
        }
    } 
}

#[test]
fn lists_compared()
{
    let mut from_list = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
        work_list : LinkedList::new(),
    };

    let mut to_list = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
        work_list : LinkedList::new(),
    };

    build_file_list(&String::from("/"),fs::read_dir(String::from("from")).unwrap(), &mut from_list);

    build_file_list(&String::from("/"),fs::read_dir(String::from("to")).unwrap(), &mut to_list);
    
    compare_hash(&mut from_list, &mut to_list);

   //assert_eq!(from_list.work_list.len(), 0);
   assert_ne!(from_list.work_list.len(), 0);

}


fn do_work(from_work: &mut BBBackupSystemFileList, from_loc: &String, to_loc: &String)
{
    println!(" ");
    if from_work.work_list.is_empty()==false
    {

        for ll_vals in from_work.work_list.iter()
        {
            println!("ll_vals.path:{:?}  to loc:{}", ll_vals, to_loc);
            let full_from_path = String::from("./") + &String::from(from_loc) + &String::from(&ll_vals.path) + &String::from(&ll_vals.filename);
            let full_to_path = String::from("./") + &String::from(to_loc) + &String::from(&ll_vals.path) + &String::from(&ll_vals.filename);
            match &ll_vals.borrow().event_type
            {
                Event::WholeBackup => 
                {
                    println!("Match Worked: whole_backup");
                    println!("{}", full_to_path);
                    let mut to_file = File::create(&full_to_path);
                    println!("{}", full_from_path);
                    fs::copy(full_from_path,&full_to_path);
                },
                Event::FolderBackup=> println!("Match Worked: FolderBackup"),
                Event::PartialBackup=> println!("Match Worked: partial_backup"),
                Event::Delete=> println!("Match Worked: delete"),
                Event::PermissionChange=>
                {
                     println!("Match Worked: permission_change");
                     let to_file = File::open(full_to_path);
                     let metadata = to_file.unwrap().metadata();
                     let mut permissions = metadata.unwrap().permissions();
                     //permissions.set_mode(0o640);
                     permissions.set_mode(ll_vals.borrow().permissions);
                     println!("{:?}",ll_vals.borrow().permissions);
                },
            }
        }
    }

    else
    {
        println!("list is empty");
    }
}


/*

backup events:
*copy whole file to backup
*permission change
*copy file change to backup
*delete file in backup

*/
fn main() 
{

    let args: Vec<String> = env::args().collect();

    if args.len()!=3
    {
        println!("usage: bback <source_folder> <backup_folder>");
        return;
    }


    let mut to_list = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
        work_list : LinkedList::new(),
    };

    let mut from_list = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
        work_list : LinkedList::new(),
    };

    build_file_list(&String::from("/"),fs::read_dir(&args[1]).unwrap(), &mut from_list);

    build_file_list(&String::from("/"),fs::read_dir(&args[2]).unwrap(), &mut to_list);
    
    compare_hash(&mut from_list, &mut to_list);

    do_work(&mut from_list, &args[1], &args[2]);

}

