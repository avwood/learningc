#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

int listsize;
char *linelist[1024];

// read a record
char* readrecord(FILE *fp)
{
  char buffer[1024];
  int length;
  char *lineptr;
  if (fgets(buffer, 1024, fp))
  {
    length = strlen(buffer);
    if (buffer[length-1]=='\n')
    {
      buffer[length--]='\0';
    }

    lineptr = malloc(length);
    memcpy(lineptr, buffer, length+1);
    return lineptr;
  }

  return NULL;
}

int main(int argc, char *argv[])
{
  char buffer[1024];

  FILE *fp;

  if (argc==2)
    {
      fp = fopen(argv[1], "r");
    }
  else if (argc==1)
    {
      fp = stdin;
    }
  else
    {
      printf("usage: readlines [filename]\n");
      return -1;
    }

  listsize = 0;
  char *line;
  int len=1;
  line = readrecord(fp);
  while( line!=NULL )
  {
    linelist[listsize] = line;
    listsize++;

    line = readrecord(fp);
  }

  int i;
  for(i=0;i<listsize;i++)
  {
    printf("line=%s", linelist[i]);
  }

}
