#include <iostream>
#include "linkedlist.hpp"

using namespace std;

void Node::setValue(char *newval)
{
    this->val = newval;
}

char* Node::getValue()
{
    return this->val;
}

Node* MyLinkedList::getFirst()
{
    return this->first;
}

void Node::setNext(Node* newnext)
{
    this->nxt = newnext;
}

Node* Node::getNext()
{
    return this->nxt;
}

void MyLinkedList::addNode(Node* newnode)
{
    newnode->setNext(first);
    first = newnode;
}

void MyLinkedList::printList()
{
    Node* nodeTmp = this->first;
    while (nodeTmp!=NULL)
    {
        cout << "node: " << nodeTmp->getValue() << endl;
        nodeTmp = nodeTmp->getNext();
    }
}

int main()
{
    Node *nodeTmp;
    MyLinkedList myList;

    nodeTmp = new Node();
    nodeTmp->setValue((char*)"hello");

    myList.addNode(nodeTmp);

    myList.printList();

    return 1;
}