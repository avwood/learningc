
#ifndef MY_LINKED_LIST
#define MY_LINKED_LIST

class Node {
    private:
        char *val;
        Node *nxt;
    public:
        void setValue(char *newval);
        char* getValue();

        void setNext(Node* newnext);
        Node* getNext();

};

class MyLinkedList {
    private:
        Node *first;

    public:
        Node* getFirst();
        void addNode(Node* newnode);
        void printList();
};

#endif