
#ifndef MY_BST
#define MY_BST

class Node {
    private:
        char *val;
        Node *left;
        Node *right;
    public:
        Node(char *newval);
        void setValue(char *newval);
        char* getValue();

        void setLeft(Node* newleft);
        Node* getLeft();
        void setRight(Node* newleft);
        Node* getRight();

};

class MyBST {
    private:
        Node *root;
        void printInOrder(Node *);
        void addNode(Node* nCur, Node* nNew);

    public:
        MyBST();
        char* getRootValue();
        void addNode(char *name);
        void printInOrder();
};

#endif