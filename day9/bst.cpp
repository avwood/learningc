#include <iostream>
#include <string.h>
#include "bst.hpp"

using namespace std;

Node::Node(char *newval)
{
    this->setValue(newval);
    this->left = NULL;
    this->right = NULL;
}

void Node::setValue(char *newval)
{
    this->val = newval;
}

char* Node::getValue()
{
    return this->val;
}

void Node::setLeft(Node* newleft)
{
    this->left = newleft;
}

Node* Node::getLeft()
{
    return this->left;
}

void Node::setRight(Node* newright)
{
    this->right = newright;
}

Node* Node::getRight()
{
    return this->right;
}

MyBST::MyBST()
{
    this->root = NULL;
}

char* MyBST::getRootValue()
{
    if (this->root!=NULL)
        return this->root->getValue();
    return "";
}

void MyBST::addNode(char* newval)
{
    Node* ntmp = new Node(newval);
    addNode(root, ntmp);

}

void MyBST::addNode(Node* nCur, Node*nNew)
{
    if (nCur==NULL)
    {
        root = nNew;
        return;
    }
    else if (strcmp(nCur->getValue(), nNew->getValue())>=0)
    {
        if (nCur->getRight()==NULL)
        {
            nCur->setRight(nNew);
            return;
        }
        else
        {
            this->addNode(nCur->getRight(), nNew);
        }
    }
    else
    {
        if (nCur->getLeft()==NULL)
        {
            nCur->setLeft(nNew);
        }
        else
        {
            this->addNode(nCur->getLeft(), nNew);
            return;
        }
    }
}

void MyBST::printInOrder()
{
    printInOrder(root);
}

void MyBST::printInOrder(Node* nCur)
{
    if (nCur->getLeft()!=NULL)
    {
        printInOrder(nCur->getLeft());
    }

    cout << "Node: " << nCur->getValue() << endl;

    if (nCur->getRight()!=NULL)
    {
        printInOrder(nCur->getRight());
    }
}

int main()
{
    Node *nodeTmp;
    MyBST tree;

    tree.addNode("martin");
    tree.addNode("sam");
    tree.addNode("mike");
    tree.addNode("adam");
    tree.addNode("zippy");

    tree.printInOrder();

    return 1;
}