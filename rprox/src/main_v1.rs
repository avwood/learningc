//use std::io::prelude::*;

use std::collections::HashMap;

use std::env;
use std::fs;
use std::io;

//use std::io::BufRead;
use std::io::BufReader;
//use std::io::BufWriter;
use std::net;
use std::sync::Arc;

use mio;
use mio::net::{TcpListener, TcpStream};

use rustls::server::{
    AllowAnyAnonymousOrAuthenticatedClient, AllowAnyAuthenticatedClient, NoClientAuth,
};
use rustls::RootCertStore;
//use rustls::verify::ClientCertVerifier;
//use rustls::{ClientCertVerifier,};
use rustls_pemfile;

/**
 * (c) Copyright 2021 Wiliam Wood Harter Jr - All Rights Reserved - Worldwide
 */

 // Token for our listening socket.
const LISTENER: mio::Token = mio::Token(0);

fn handle_client(listener: TcpListener, registry: &mio::Registry)  -> io::Result<()> 
{
    loop {
        match listener.accept() {
            Ok((socket,addr)) => {
                print!("new connectionfrom {}", addr);
            }
            Err(ref err)
                if err.kind()==io::ErrorKind::WouldBlock => return Ok(()),
            Err(err) => {
                println!("error accepting connection. err={}", err);
                return Err(err);
            }
        }
    }

    /*
    let mut buf:String = String::new();
    let rs:usize;
    let mut reader = BufReader::new(&stream);
    let mut writer = BufWriter::new(&stream);

    rs = reader.read_line(&mut buf).unwrap();
    print!("size:{} read: {}", rs, buf);

    writer.write_all(format!("Thank you for the text back at you: {}",buf).as_bytes())?;
    writer.flush()?;
    Ok(())
    */
}

fn connection_event(registry: &mio::Registry, event: &mio::event::Event) {
    let token = event.token();

    print!("Got a connection event token: {:?}", token);
    /*
    if self.connections.contains_key(&token) {
        self.connections
            .get_mut(&token)
            .unwrap()
            .ready(registry, event);

        if self.connections[&token].is_closed() {
            self.connections.remove(&token);
        }
    }
    */
}

struct OpenConnection {
    socket: TcpStream,
    token: mio::Token,
    closing: bool,
    closed: bool,
    tls_conn: rustls::ServerConnection,
    to_stream: TcpStream,
    sent_http_response: bool,
}

impl OpenConnection {
    fn new(
        socket: TcpStream,
        token: mio::Token,
        tls_conn: rustls::ServerConnection,
        port: u16,
    ) -> OpenConnection {
        let addr = net::SocketAddrV4::new(net::Ipv4Addr::new(127, 0, 0, 1), port);
        let to_stream = TcpStream::connect(net::SocketAddr::V4(addr)).unwrap();
        OpenConnection {
            socket,
            token,
            closing: false,
            closed: false,
            tls_conn,
            to_stream,
            sent_http_response: false,
        }
    }

    /// What IO events we're currently waiting for,
    /// based on wants_read/wants_write.
    fn event_set(&self) -> mio::Interest {
        let rd = self.tls_conn.wants_read();
        let wr = self.tls_conn.wants_write();

        if rd && wr {
            mio::Interest::READABLE | mio::Interest::WRITABLE
        } else if wr {
            mio::Interest::WRITABLE
        } else {
            mio::Interest::READABLE
        }
    }
}

fn make_config(/*args: &Args*/) -> Arc<rustls::ServerConfig> {
    // TODO, this root certificate needs investigating
    let use_client_auth = false;
    let client_auth; //:Arc<dyn ClientCertVerifier>;
    if use_client_auth {
        let root_certfile = fs::File::open("<where is this cert file?").expect("cannot open certificate file");
        let mut reader = BufReader::new(root_certfile);
        let roots:Vec<rustls::Certificate> = rustls_pemfile::certs(&mut reader)
            .unwrap()
            .iter()
            .map(|v| rustls::Certificate(v.clone()))
            .collect();
        let mut client_auth_roots = RootCertStore::empty();
        for root in roots {
            client_auth_roots.add(&root).unwrap();
        }
        let require_auth = false;
        if require_auth {
            client_auth = AllowAnyAuthenticatedClient::new(client_auth_roots)
        } else {
            client_auth = AllowAnyAnonymousOrAuthenticatedClient::new(client_auth_roots)
        }
    } else {
        client_auth = NoClientAuth::new()
    };

    //let specify_cipher_suites = false;
    // if we are going to allow specify_suites, filter them from in this vec
    let suites = rustls::ALL_CIPHER_SUITES.to_vec();

    let specify_versions = false;
    // if we are going to allow specific versions, filter them from this vec
    let versions = rustls::ALL_VERSIONS.to_vec();

    let certfile = fs::File::open("./cert/danny.crt").expect("cannot open certificate file");
    let mut reader = BufReader::new(certfile);
    let certs = rustls_pemfile::certs(&mut reader)
        .unwrap()
        .iter()
        .map(|v| rustls::Certificate(v.clone()))
        .collect();

    let keyfilename = "./cert/danny.key";
    let keyfile = fs::File::open(keyfilename).expect("cannot open private key file");
    let mut reader = BufReader::new(keyfile);
    let privkey: Option<rustls::PrivateKey>;
    loop {
        match rustls_pemfile::read_one(&mut reader).expect("cannot parse private key .pem file") {
            Some(rustls_pemfile::Item::RSAKey(key)) => {println!("got an RSA key"); privkey = Some(rustls::PrivateKey(key)); break;},
            Some(rustls_pemfile::Item::PKCS8Key(key)) => {println!("got a pkcs key"); privkey = Some(rustls::PrivateKey(key)); break;},
            None =>{privkey = None; break},
            _ => ()
        }
    }

    if privkey.is_none() {
        panic!(
            "no keys found in {:?} (encrypted keys not supported)",
            keyfilename
        );    
    }

    let ocsp = Vec::new();

    let mut config = rustls::ServerConfig::builder()
        .with_cipher_suites(&suites)
        .with_safe_default_kx_groups()
        .with_protocol_versions(&versions)
        .expect("inconsistent cipher-suites/versions specified")
        .with_client_cert_verifier(client_auth)
        .with_single_cert_with_ocsp_and_sct(certs, privkey.unwrap(), ocsp, vec![])
        .expect("bad certificates/private key");

    config.key_log = Arc::new(rustls::KeyLogFile::new());

    let flag_resumption = false;
    if flag_resumption {
        config.session_storage = rustls::server::ServerSessionMemoryCache::new(256);
    }

    let flag_tickets = false;
    if flag_tickets {
        config.ticketer = rustls::Ticketer::new().unwrap();
    }

    let flag_proto = ["forward"];
    config.alpn_protocols = flag_proto
        .iter()
        .map(|proto| proto.as_bytes().to_vec())
        .collect::<Vec<_>>();

    Arc::new(config)
}

fn main() -> std::io::Result<()> {

    let mut connections: HashMap<mio::Token, OpenConnection> = HashMap::new();

    let args: Vec<String> = env::args().collect();

    let mut addr: net::SocketAddr = "0.0.0.0:4040".parse().unwrap();
    //addr.set_port(args.flag_port.unwrap_or(443));
    addr.set_port(4040);

    //let listener = TcpListener::bind("0.0.0.0:4040")?;
    let mut listener = TcpListener::bind(addr).expect("cannot listen on port");
    let mut poll = mio::Poll::new().unwrap();
    poll.registry()
        .register(&mut listener, LISTENER, mio::Interest::READABLE)
        .unwrap();


    print!("listening on: {}\n", listener.local_addr().unwrap());

    let mut events = mio::Events::with_capacity(256);

    let mut next_id = 0;

    loop {
        poll.poll(&mut events, None).unwrap();

        for event in events.iter() {
            match event.token() {
                LISTENER => {
                    match listener.accept() {
                        Ok((socket, addr)) => {
                            println!("Accepting new connection from {:?}", addr);

                            let tls_config = make_config();
                        
                            let tls_conn = 
                                rustls::ServerConnection::new(Arc::clone(&tls_config)).unwrap();
        
                            let token = mio::Token(next_id);
                            next_id += 1;
        
                            let mut connection = OpenConnection::new(socket, token, tls_conn, 8080);
                            let eset =  connection.event_set();
                            poll.registry()
                            .register(&mut connection.socket, token, eset)
                            .unwrap();

                            connections
                                .insert(token, connection);                        
                        }
                        Err(ref err) if err.kind() == io::ErrorKind::WouldBlock => return Ok(()),
                        Err(err) => {
                            println!(
                                "encountered error while accepting connection; err={:?}",
                                err
                            );
                            return Err(err);
                        }
                    }
                    //handle_client(listener, poll.registry());
                    //tlsserv
                    //    .accept(poll.registry())
                    //    .expect("error accepting socket");
                }
                _ => {println!("got a connection event"); connection_event(poll.registry(), &event);},
            }
        }
    }

    // accept connections and process them serially
    /*
    for stream in listener.incoming() {
        handle_client(stream?)?;
    }
    */

    //Ok(())
}