use crypto::digest::Digest;
use crypto::sha1::Sha1;
use std::env;
use std::fs;
use std::io::Read;

const BUFFER_SIZE: usize = 1024;

/// Compute digest value sh for given reader
/// On any error simply return without doing anything
fn calc_checksum<D: Digest, R: Read>(reader: &mut R, sh: &mut D) {
    let mut buffer = [0u8; BUFFER_SIZE];
    loop {
        let n = match reader.read(&mut buffer) {
            Ok(n) => n,
            Err(_) => return,
        };
        sh.input(&buffer[..n]);
        if n == 0 || n < BUFFER_SIZE {
            break;
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    let path = &args[1];
    if let Ok(mut file) = fs::File::open(&path) {
        let mut cs = Sha1::new();
        calc_checksum(&mut file, &mut cs);
        println!("{}\t{}",cs.result_str(), path);
        //print_result(d.result_str(), path);

    }
}
