#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int addtwonums(int a, int b)
{
  return a + b;
}

int main()
{
  char buffer[1024];

  int z = addtwonums(40, 2);
  int fd = open("data.txt", O_RDONLY);

  printf("fd=%d\n",fd);

  read(fd, &buffer, 10);
  buffer[10]='\0';
  printf("read=%s\n", buffer);

  int i;
  for (i=0;i<10;i++)
  {
    printf("c[%d] = %d\n", i, (int)buffer[i]);
  }
}
