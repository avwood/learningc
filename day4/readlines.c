#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

char* readrecord(FILE *fp)
{
  char buffer[1024];
  char *lineptr;
  if (fgets(buffer, 1024, fp))
    {
      printf("buffer=%s   len=%d\n", buffer, strlen(buffer));
      lineptr = malloc(strlen(buffer));
      strcpy(lineptr, buffer);
      return lineptr;
    }

  return NULL;
}

int main(int argc, char *argv[])
{
  char buffer[1024];

  FILE *fp;

  if (argc==2)
    {
      fp = fopen(argv[1], "r");
    }
  else if (argc==1)
    {
      fp = stdin;
    }
  else
    {
      printf("usage: readlines [filename]\n");
      return -1;
    }

  char *line;
  int len=1;
  line = readrecord(fp);
  while( line!=NULL )
  {
    printf("line=%s", line);
    free(line);
    line = readrecord(fp);
  }
}
