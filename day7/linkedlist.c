#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

struct node {
  char* name;
  int linenum;
  struct node* nxt;
};

/**
 * addnode
 * params:
 *   list - the pointer to the first element in the list
 *   newnode - the new node to add
 * return:
 *   returns the current poiter to the list
 */
struct node* addnode(struct node* list, struct node* newnode)
{
  newnode->nxt = list;
  list = newnode;
  return list;
}

// function to append to the list

// function to remove all elements in the list

// function to find an element in a list

// function to sort the list

/**
 * recursive print of the list
 */
void printlist(struct node *list)
{
  if (list->nxt!=NULL)
  {
    printlist(list->nxt);
  }
  printf("%s ", list->name);
}

/**
 * reverse recursive print of the list
 */

void printlistreverse(struct node *list)
{
  printf("%s ", list->name);
  if (list->nxt!=NULL)
  {
    printlistreverse(list->nxt);
  }
}


/**
 * readrecord
 * params:
 *   File *fp
 *   int count;  this is the line number for counting purposes
 *
 * returns:
 *   a pointer to node allocated on the heap. On error or eof returns NULL
*/
struct node* readrecord(FILE *fp, int count)
{
  char buffer[1024];
  int length;
  char *lineptr;
  struct node* newnode;
  struct node localnode;

  if (fgets(buffer, 1024, fp))
  {
    length = strlen(buffer);
    if (buffer[length-1]=='\n')
    {
      buffer[length--]='\0';
    }


    printf("node size = %d\n", sizeof(struct node));
    newnode = malloc(sizeof(struct node));
    lineptr = malloc(length);
    memcpy(lineptr, buffer, length+1);
    newnode->name = lineptr;
    newnode->linenum = count;

    return newnode;
  }

  return NULL;
}

int main(int argc, char *argv[])
{
  char buffer[1024];
  struct node* firstnode=NULL;
  struct node* curnode;
  struct node* tmpnode;
  int linecount = 0;
  FILE *fp;

  if (argc==2)
    {
      fp = fopen(argv[1], "r");
      if (fp==NULL)
      {
        printf("file doesn't exist: %s\n", argv[1]);
        return -1;
      }
    }
  else if (argc==1)
    {
      fp = stdin;
    }
  else
    {
      printf("usage: linkedlist [filename]\n");
      return -1;
    }

  curnode = readrecord(fp, linecount);
  while( curnode!=NULL )
  {
    firstnode = addnode(firstnode, curnode);

    linecount++;

    curnode = readrecord(fp, linecount);
  }

  printf("regular print\n");
  printlist(firstnode);

  printf("reverse print\n");
  printlistreverse(firstnode);
 

}
