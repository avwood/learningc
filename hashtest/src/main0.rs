use std::collections::HashMap;
use std::env;
use std::fs;
use std::rc::Rc;

#[derive(Debug)]
struct BBFileInfo {
    path: String,
    filename: String,
    size: usize,
    checksum: u64,
}

struct BBBackupSystemFileList {
     folderlist: HashMap<String, String>,
}

#[warn(non_snake_case)]
fn main() 
{

    let mut a = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
    };
    a.folderlist.insert(String::from("42"),String::from("hello"));
    let Rc_a= Rc::new(a);
    println!("count after creating a = {}", Rc::strong_count(&Rc_a));

    

    let b = Rc::clone(&Rc_a);
    println!("count after creating b = {}", Rc::strong_count(&Rc_a));

    {
        let c = Rc::clone(&Rc_a); 
        println!("count after creating c = {}", Rc::strong_count(&Rc_a));
    }

    println!("count after c goes out fo scope = {}", Rc::strong_count(&Rc_a));

    println!(" ");
    println!("Rc_a:");
    for (key, value) in &Rc_a.folderlist {
        println!("{}: {}", key, value);
    }
   
    println!(" ");
    println!("b:");
    for (key, value) in &b.folderlist {
        println!("{}: {}", key, value);
    }


}