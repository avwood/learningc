
use std::cell::RefCell;
use std::collections::HashMap;

#[derive(Debug)]
struct BBFileInfo 
{
    path: String,
    filename: String,
}

struct BBBackupSystemFileList 
{
    folderlist: HashMap<String, RefCell<HashMap<String, RefCell<BBFileInfo>>>>,
}

impl BBBackupSystemFileList{
    pub fn add_file(& mut self, loc:String,filename:String) {
        let fi = BBFileInfo  {
            path: loc.clone(),
            filename: filename.clone(),
        };
        if self.folderlist.contains_key(&loc) {
            self.folderlist.get(&loc).unwrap().borrow_mut().insert(filename, RefCell::new(fi));
        }
        else
        {
            let mut a=HashMap::new();
            a.insert(filename.clone(), RefCell::new(fi));
            self.folderlist.insert(loc, RefCell::new(a));
        }
    }
}

#[test]
fn adds_folder()
{
    let mut h=BBBackupSystemFileList
    {
        folderlist: HashMap::new(),
    };
    h.add_file(String::from("./"),String::from("newfile.txt"));

    assert_eq!(h.folderlist.len(),1,"The file count was not 1");

    let fname = h.folderlist.get("./").unwrap().borrow_mut().get("newfile.txt").unwrap().borrow().filename.clone();
    assert_eq!(fname, "newfile.txt","The filename didn't match");
}


#[warn(non_snake_case)]
fn main() 
{
    
    let mut h=BBBackupSystemFileList
    {
        folderlist: HashMap::new(),
    };

    let mut a: HashMap<String, RefCell<BBFileInfo>>; 
    a=HashMap::new();



    let mut fi=BBFileInfo
    {
        path: String::from("./"),
        filename: String::from("file_a.txt"),
    };

    a.insert(String::from("file_a.txt"), RefCell::new(fi));

    fi=BBFileInfo
    {
        path: String::from("./"),
        filename: String::from("file_b.txt"),
    };

    a.insert(String::from("file_b.txt"), RefCell::new(fi));
    
    

    //let Rc_a= Rc::new(a);
    //h.folderlist=HashMap::new();
    {
    h.folderlist.insert(String::from("./"),RefCell::new(a));

    let mut c = h.folderlist.get(&String::from("./")).unwrap().borrow_mut();

    fi=BBFileInfo
    {
        path: String::from("./"),
        filename: String::from("file_c.txt"),
    };

    //h.folderlist.get(&String::from("./")).unwrap().borrow_mut().insert(String::from("file_c.txt"), RefCell::new(fi));
    c.insert(String::from("file_c.txt"), RefCell::new(fi));
    }

    for (key, value) in h.folderlist
    {
        for (k2, v2) in value.borrow_mut().iter_mut()
        {
            println!("{}: {}: {}", key, k2, v2.borrow().filename) 
        }
    }


}

