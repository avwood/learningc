
use std::collections::HashMap;
use std::env;
use std::fs;
use std::rc::Rc;

#[derive(Debug)]
struct BBFileInfo {
    path: String,
    filename: String,
    size: usize,
    checksum: u64,
}

struct BBBackupSystemFileList {
     folderlist: HashMap<String, String>,
}

#[warn(non_snake_case)]
fn main() 
{

    let mut h:HashMap<String, Rc<BBBackupSystemFileList>>;

    let mut a = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
    };

    let mut b = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
    };

    let mut c = BBBackupSystemFileList 
    {
        folderlist : HashMap::new(),
    };

    a.folderlist.insert(String::from("42"), String::from("Wood"));
    a.folderlist.insert(String::from("43"), String::from("Danny"));

    //let Rc_a= Rc::new(a);
    h=HashMap::new();
    h.insert(String::from("a"),Rc::new(a));
    h.insert(String::from("b"),Rc::new(b));
    h.insert(String::from("c"),Rc::new(c));

    let r: &Rc<BBBackupSystemFileList>;

    r = h.get(&String::from("a")).unwrap();

    println!("count after r = {}", Rc::strong_count(&r));

    let bbsfl: &BBBackupSystemFileList;

    bbsfl= r.as_ref();

    for (key, value) in &bbsfl.folderlist {
        println!("{}: {}", key, value);
    }


}

