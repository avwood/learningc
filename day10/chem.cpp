#include <iostream>  
#include <fstream>
#include <vector>
using namespace std;
#include "chem.hpp"


Element::Element(string name, int anum)
{
    this->name = name;
    this->atomic_number = anum;
}

string Element::getName()
{
    return this->name;
}
int Element::getAtomicNumber()
{
    return this->atomic_number;
}

ostream& operator<<(ostream &out, Element* e)
{
    out << e->getName();
    return out;
}

int main () {  
  vector<Element*> vnames;
  string srg;  
  int i = 0;
  ifstream filestream("elements.txt"); 
  if (filestream.is_open())  
  { 
    while ( getline (filestream,srg) )  
    {  
      vnames.push_back(new Element(srg,i++));
      //cout << srg <<endl;
    }  
    filestream.close();  
  }  
  else {  
      cout << "File opening is fail."<<endl;   
    }

  for(vector<Element*>::iterator itr=vnames.begin();itr!=vnames.end();++itr)  
    cout<<*itr<<endl;  
  return 0;  
}  