#ifndef CHEM_HPP
#define CHEM_HPP 1
class Element {
    private:
        string name;
        int valance;
        int atomic_number;
    public:
        Element(string name, int anum);
        string getName();
        int getAtomicNumber();
        friend std::ostream& operator<<(std::ostream &out, Element e);
};
#endif