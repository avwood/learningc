extern crate postgres;

use postgres::{Connection, TlsMode};

struct OwnerInfo {
    name: String,
    owner_email: String,
    owner_id: i32,
    
}

fn main() {
    let conn = Connection::connect("postgresql://postgres@localhost:5433", TlsMode::None)
            .unwrap();

    /*
    conn.execute("CREATE TABLE person (
                    id              SERIAL PRIMARY KEY,
                    name            VARCHAR NOT NULL,
                    data            BYTEA
                  )", &[]).unwrap();
    let me = Person {
        id: 0,
        name: "Steven".to_owned(),
        data: None
    };
    
    conn.execute("INSERT INTO person (name, data) VALUES ($1, $2)",
                 &[&"danny", &"danny@bittymail.com"]).unwrap();
    */

    for row in &conn.query("SELECT (name, owner_email, owner_id)  FROM owner_info", &[]).unwrap() {
        let person = OwnerInfo {
            name: row.get(1),
            owner_email: row.get(2),
            owner_id: row.get(0),
        };
        println!("Found person {}", person.name);
    }
}